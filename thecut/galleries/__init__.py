# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


__title__ = 'thecut-galleries'

__version__ = '0.1.1'

__url__ = 'https://github.com/thecut/thecut-galleries'

__author__ = 'The Cut Creative <development@thecut.net.au>'

__copyright__ = 'Copyright 2017 Busara Perth Pty Ltd'

__license__ = 'Apache 2.0'

default_app_config = 'thecut.galleries.apps.AppConfig'
