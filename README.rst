=================
thecut-galleries
=================

.. image:: https://travis-ci.org/thecut/thecut-galleries.svg
    :target: https://travis-ci.org/thecut/thecut-galleries

.. image:: https://codecov.io/github/thecut/thecut-galleries/coverage.svg
    :target: https://codecov.io/github/thecut/thecut-galleries

.. image:: https://readthedocs.org/projects/thecut-galleries/badge/?version=latest
    :target: http://thecut-galleries.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status


Django media galleries.


.. Features
.. --------
..
..   *


Documentation
-------------

The full documentation is at https://thecut-galleries.readthedocs.org.


Quickstart
----------

Install ``thecut-galleries`` using the installation instructions found in the project documentation.


Credits
-------

See ``AUTHORS.rst``.
